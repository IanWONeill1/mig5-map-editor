package org.mapeditor.client;

/* Class58 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Timer {
	
    abstract int sleep(int sleepTimer, int i_1_);
    
    abstract void start();
    
    public Timer() {
	/* empty */
    }
    
    abstract void reset();
}
