package org.mapeditor.client;

/* Class21 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public final class DecoratedTile {
	
    public int texture;
    public int anInt374;
    public int anInt375;
    public int anInt380;
    public int anInt381;
    public int minimapColor;
    public boolean aBoolean383 = true;
    
    public DecoratedTile(int i, int i_13_, int i_15_, int i_14_, int i_16_, int i_17_, boolean bool) {
    	anInt381 = i_13_;
    	anInt380 = i;
    	texture = i_16_;
    	aBoolean383 = bool;
    	anInt375 = i_15_;
    	anInt374 = i_14_;
    	minimapColor = i_17_;
    }
}
